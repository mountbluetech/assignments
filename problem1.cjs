
function findCarByID(inventory,checkID){
    if(!inventory || inventory.length == 0 || !Array.isArray(inventory)) return [];
    else if(typeof(checkID) !== 'number' || checkID === null || checkID === undefined || checkID==='') return [];
    for (let ID = 0; ID < inventory.length; ID++) {
        if (inventory[ID]['id'] === checkID) {
            let carID = inventory[ID]['id'];
        let year = inventory[ID].car_year;
        let make = inventory[ID].car_make;
        let model = inventory[ID].car_model;
        return inventory[ID];
        }
    }
}



module.exports = findCarByID;
