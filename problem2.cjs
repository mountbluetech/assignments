function lastCarInInventory(inventory) {
    if(!inventory || inventory.length == 0 || !Array.isArray(inventory)) return [];
    else {
       let lastCar = inventory[inventory.length - 1];
       return 'Last car is a ' + lastCar.car_make +' '+ lastCar.car_model;
    }
}

module.exports = lastCarInInventory;