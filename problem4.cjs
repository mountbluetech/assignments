
function getYearsOfAllCars(inventory){
    let years = [];
    if(!inventory || inventory.length == 0 || !Array.isArray(inventory)) return [];
    else
    for(let carID=0;carID<inventory.length;carID++){
        years.push(inventory[carID]['car_year']);
    }
    return years;
}

module.exports = getYearsOfAllCars;