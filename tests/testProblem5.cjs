let Inventory = require('../inventory.cjs');
let carsOlderThanYear = require('../problem5.cjs');
let getYearsOfAllCars = require('../problem4.cjs');
let years = getYearsOfAllCars(Inventory);

const olderCars = carsOlderThanYear(Inventory, years, 2000);
console.log(olderCars);
console.log(olderCars.length)

const olderthan1995 = carsOlderThanYear(Inventory, years, 1995);
console.log(olderthan1995);
console.log(olderthan1995.length)

const emptyCars = carsOlderThanYear(Inventory,[2015,1995,2000,2007],2010);
console.log(emptyCars);
console.log(emptyCars.length)

const noArguments = carsOlderThanYear();
console.log(noArguments);
console.log(noArguments.length)