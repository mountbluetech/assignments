
function getBMWandAudicars(inventory){
    let bmwandAudi = [];
    if(!inventory || inventory.length == 0 || !Array.isArray(inventory)) return [];
    else
    for(let index = 0 ; index < inventory.length ; index++){
        if(inventory[index]['car_make'] === 'Audi' || inventory[index]['car_make'] === 'BMW'){
            bmwandAudi.push(inventory[index]);
        }
    }
    return bmwandAudi;
}

module.exports = getBMWandAudicars;