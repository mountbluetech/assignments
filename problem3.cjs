
function sortInventorybyCarModel(inventory) {
    if(!inventory || inventory.length == 0 || !Array.isArray(inventory)) return [];
    inventory.sort(function(a,b) {
        return a['car_model'] < b['car_model']?-1:a['car_model'] > b['car_model']?1:0;
    })
    return inventory;
}

module.exports = sortInventorybyCarModel;