
function carsOlderThanYear(inventory,years,year){
    let carsTillYear = [];
    if(!inventory || inventory.length == 0 || !Array.isArray(inventory)) return [];
    else if(years.length === 0 || !Array.isArray(years)) return [];
    else {
    for(let ele=0;ele<years.length;ele++){
        if(years[ele]<year) carsTillYear.push(years[ele]);
    }
}
     return carsTillYear;
}

module.exports = carsOlderThanYear;